

/*__________ Owl Carousel Main Banner +++ __________*/
$(document).ready(function () {
	try {
		var owl = $('.bnr_main');

		owl.owlCarousel({
			// autoPlay: 5000,
			autoHeight: true,
			singleItem: true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			transitionStyle: 'backSlide',
			lazyLoad: true
		});
	}
	catch(e) {
		console.warn('Owl Carousel cannot find ".bnr_main"');
	}
});
/*__________ Owl Carousel Main Banner --- __________*/




/*__________ Owl Carousel Banner Ad +++ __________*/
$(document).ready(function () {
	try {
		var owl = $('.bnr_ad');

		owl.owlCarousel({
			// autoPlay: 5000,
			autoHeight: true,
			singleItem: true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			lazyLoad: true
		});
	}
	catch (e) {
		console.warn('Owl Carousel cannot find ".bnr_ad"');
	}
});
/*__________ Owl Carousel Banner Ad --- __________*/




/*__________ Cart +++ __________*/
$(document).on('click', '.prd_new_cart', function () {
	$(this).addClass('active');
	// $('.hdr_cart_inr > .ico').toggleClass('active');
	//
	// if ($('.prd_new_cart.active').length > 0)
	// {
	// 	$('.hdr_cart_inr > .num').removeClass('hidden').text($('.prd_new_cart.active').length);
	// }
	// else
	// {
	// 	$('.hdr_cart_inr > .num').addClass('hidden');
	// }

	return false;
});
/*__________ Cart --- __________*/





/*__________ Search Button +++ __________*/
$(document).on('click', '.hdr_srch > a', function (e) {
	e.preventDefault();

	$('html, body').addClass('ov-h');
	$('.search_sec').addClass('active');
});

$(document).on('click', '.search_btn_cls > button', function (e) {
	e.preventDefault();

	$('html, body').removeClass('ov-h');
	$('.search_sec').removeClass('active');
});
/*__________ Search Button --- __________*/





/*__________ Footer Menu +++ __________*/
$(document).on('click', '.ftr_menu_ls > li > a', function () {
	$(this).parent().toggleClass('active');
	$(this).siblings('.drop').slideToggle();

	return false;
});
/*__________ Footer Menu --- __________*/





/*__________ Navigation Menu +++ __________*/
$(document).on('click', '.nav_btn_open > a, .nav_menu_bg', function () {
	$('.wrapper_cnt').toggleClass('active');
	$('.nav_menu_sec').toggleClass('active');
	$('.nav_menu_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');

	return false;
});
/*__________ Navigation Menu --- __________*/





/*__________ Navigation Sub Menu +++ __________*/
$(document).on('click', '.nav_menu_ctgr_ls > li > a', function () {
	$('.nav_menu_sub_ctgr').eq($(this).parent().index()).addClass('active').siblings().removeClass('active');
	$('.nav_menu_sub_lang').removeClass('active');

	$('.nav_menu_bx').addClass('active_1');

	return false;
});
/*__________ Navigation Sub Menu --- __________*/




/*__________ Navigation Menu Language +++ __________*/
$(document).on('click', '.nav_menu_lang > a', function () {
	$('.nav_menu_sub_lang').addClass('active');
	$('.nav_menu_sub_ctgr').removeClass('active');

	$('.nav_menu_bx').addClass('active_1');

	return false;
});

$(document).on('click', '.nav_menu_sub_lang_ls > li > a', function () {
	$(this).parent().addClass('active').siblings().removeClass('active');
	$('.nav_menu_lang > a > .val > .txt').text($(this).text());

	$('.nav_menu_bx').removeClass('active_1');

	return false;
});
/*__________ Navigation Menu Language --- __________*/




/*__________ Navigation Sub Back +++ __________*/
$(document).on('click', '.nav_menu_sub_back > a', function () {
	$('.nav_menu_bx').removeClass('active_1');

	return false;
});
/*__________ Navigation Sub Back --- __________*/




/*__________ Products Filter Select +++ __________*/
$(document).on('click', '.prds_ftr_sel_ttl', function () {
	$(this).parent().toggleClass('active');
});

$(document).on('click', function(e) {
	if ($(e.target).is('.prds_ftr_sel_ttl, .prds_ftr_sel_ttl *') === false) {
		$('.prds_ftr_sel').removeClass('active');
	}
});

// $(document).on('click', '.prds_ftr_sel_ls > li > a', function (e) {
// 	e.preventDefault();
//
// 	$('.prds_ftr_sel_name').html($(this).html());
// });
/*__________ Products Filter Select --- __________*/




/*__________ Products Navigation More +++ __________*/
$(document).on('click', '.prds_nav_ftr_chk_mr > a', function (e) {
	e.preventDefault();

	$(this).closest('li').addClass('hidden').siblings().removeClass('hidden');
});
/*__________ Products Navigation More --- __________*/




/*__________ Products Navigation Collapse +++ __________*/
$(document).on('click', '.prds_nav_ftr_ttl', function () {
	$(this).parent().toggleClass('active');
	$(this).siblings('.prds_nav_ftr_cnt').slideToggle();
});
/*__________ Products Navigation Collapse --- __________*/





/*__________ Products Range Slider +++ __________*/
try {
	var rangeSlider = document.getElementsByClassName("prds_nav_ftr_range")[0];

	noUiSlider.create(rangeSlider, {
		start: [2000000, 5000000],
		step: 1000,
		connect: true,
		range: {
			'min': 0,
			'max': 10000000
		},
		format: wNumb({
			decimals: 0,
			thousand: ' ',
			postfix: ' so`m'
		})
	});


	var rangeInputFrom = document.querySelectorAll('.prds_nav_ftr_prc_inp > input')[0];
	var rangeInputTo = document.querySelectorAll('.prds_nav_ftr_prc_inp > input')[1];

	rangeSlider.noUiSlider.on('update', function( values, handle ) {

		var value = values[handle];

		if ( handle ) {
			rangeInputTo.value = value;
		}
		else {
			rangeInputFrom.value = value;
		}
	});

	rangeInputTo.addEventListener('change', function(){
		rangeSlider.noUiSlider.set([null, this.value]);
	});

	rangeInputFrom.addEventListener('change', function(){
		rangeSlider.noUiSlider.set([this.value, null]);
	});
}
catch(e) {
	console.warn("Range Slider cannot find elements");
}
/*__________ Products Range Slider --- __________*/




/*__________ Products Filter Reset +++ __________*/
$(document).on('click', '.prds_nav_btn > li.clean > .btn', function (e) {
	e.preventDefault();

	$(this).closest('.prds_nav').find('.prds_nav_ftr_chk > input').prop('checked', false);
	document.getElementsByClassName("prds_nav_ftr_range")[0].noUiSlider.reset();
});
/*__________ Products Filter Reset --- __________*/



/*__________ Products Navigation Show +++ __________*/
$(document).on('click', '.prds_ftr_btn > a, .prds_ftr_nav_bg, .prds_nav_btn > li.close > .btn', function (e) {
	e.preventDefault();

	$('.wrapper_cnt').toggleClass('active');
	$('.prds_nav_sec').toggleClass('active');
	$('.prds_ftr_nav_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');
});



$(document).ready(function () {
	$('.prds_nav_sec').insertAfter($('.nav_menu_sec'));
});
/*__________ Products Navigation Show --- __________*/






/*__________ Owl Carousel Main Banner +++ __________*/
$(document).ready(function () {
	try {
		var owl = $('.prd_prev_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			autoHeight: true,
			singleItem: true,
			navigation: false,
			// pagination: false,
			slideSpeed: 400,
			lazyLoad: true
		});
	}
	catch(e) {
		console.warn('Owl Carousel cannot find ".prd_prev_bnr"');
	}
});
/*__________ Owl Carousel Main Banner --- __________*/




/*__________ Product Description +++ __________*/
$(document).on('click', '.prd_desc_nav_ls > li > a', function (e) {
	e.preventDefault();

	$(this).parent().addClass('active').siblings().removeClass('active');

	$('.prd_desc_it').eq($(this).parent().index()).addClass('active').siblings().removeClass('active');

});
/*__________ Product Description --- __________*/





/*__________ International Tel Input +++ __________*/
try {
	$('.int_tel').intlTelInput({
		// allowDropdown: false,
		// autoHideDialCode: false,
		// autoPlaceholder: "off",
		// dropdownContainer: "body",
		// excludeCountries: ["us"],
		// formatOnDisplay: false,
		geoIpLookup: function(callback) {
			$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		// initialCountry: "auto",
		// nationalMode: false,
		onlyCountries: ['uz', 'ru', 'en', 'kz', 'kg', 'kr'],
		// placeholderNumberType: "MOBILE",
		preferredCountries: ['uz'],
		// separateDialCode: true,
		// utilsScript: "lib/int_tel_input/js/utils.js"
	});
}
catch(e) {
	console.warn('International Tel Input cannot find ".int_tel"');
}
/*__________ International Tel Input --- __________*/





/*__________ Delivery Tab +++ __________*/
$(document).on('click', '.dlvr_drd_ttl', function () {
	$(this).toggleClass('active');
	$(this).siblings('.dlvr_drd_cnt').slideToggle();
});
/*__________ Delivery Tab --- __________*/




/*__________ Feedback Star +++ __________*/
$(document).on('click', '.fbk_rt_star > li > a', function (e) {
	e.preventDefault();

	$('.fbk_rt_star > li').removeClass('active');
	$(this).parent().addClass('active').prevAll().addClass('active');
});
/*__________ Feedback Star --- __________*/



/*__________ Feedback Photo Upload +++ __________*/
$('.fbk_img_up_btn > input').on('change', function () {

	if (this.files && this.files[0]) {
		var reader = new FileReader();

		reader.onload = function () {
			var imageItem =
				'<li>' +
					'<div class="fbk_img_up_pic inr-c">' +
						'<img src="' + reader.result +'" alt="">' +
					'</div>' +
					'<div class="fbk_img_up_del">' +
						'<a class="mbl_click_eff" href="">Удалить</a>' +
					'</div>' +
				'</li>';

			$('.fbk_img_up_ls').prepend(imageItem);
		};

		reader.readAsDataURL(this.files[0]);
	}
	else {
		console.log('Not uploaded');
	}
});
/*__________ Feedback Photo Upload --- __________*/




/*__________ Feedback Photo Delete +++ __________*/
$(document).on('click', '.fbk_img_up_del > a', function (e) {
	e.preventDefault();

	$(this).closest('li').remove();
});
/*__________ Feedback Photo Delete --- __________*/




/*__________ PC Message Action +++ __________*/
$(document).on('click', '.pc_msg_in_act_btn', function (e) {
	e.preventDefault();

	$(this).parent().toggleClass('active');
});


$(document).on('click', function(e) {
	if ($(e.target).is('.pc_msg_in_act_btn, .pc_msg_in_act_btn *') === false) {
		$('.pc_msg_in_act').removeClass('active');
	}
});
/*__________ PC Message Action --- __________*/





/*__________ Feedback Photo Upload +++ __________*/
$('.pc_prf_img_inp > input').on('change', function () {

	if (this.files && this.files[0]) {
		var reader = new FileReader();

		reader.onload = function () {

			$('.pc_prf_img > .img').attr('style', 'background-image: url(' + reader.result + ')');

		};

		reader.readAsDataURL(this.files[0]);
	}
	else {
		console.log('Not uploaded');
	}
});
/*__________ Feedback Photo Upload --- __________*/




/*__________ Owl Carousel Seller Banner +++ __________*/
$(document).ready(function () {
	try {
		var owl = $('.shp_slr_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			autoHeight: true,
			singleItem: true,
			navigation: false,
			// pagination: false,
			slideSpeed: 400,
			transitionStyle: 'backSlide',
			lazyLoad: true
		});
	}
	catch(e) {
		console.warn('Owl Carousel cannot find ".shp_slr_bnr"');
	}
});
/*__________ Owl Carousel Seller Banner --- __________*/






/*__________ Password Recovery +++ __________*/
$(document).on('change', '.pass_rec_rdo > input', function () {
	$('.pass_rec_opt_it').removeClass('active');

	if ($(this).parent().hasClass('tel'))
	{
		$('.pass_rec_opt_it.tel').addClass('active');
	}
	else if ($(this).parent().hasClass('email'))
	{
		$('.pass_rec_opt_it.email').addClass('active');
	}
});
/*__________ Password Recovery --- __________*/





/*__________ Shop Seller Slide Input +++ __________*/
$(document).on('change', '.slr_shp_stg_sld_inp > .hdn', function () {
	var fileName = $(this).val().split("\\").pop();

	if (fileName == "")
	{
		$(this).siblings(".vsbl").val("Формат jpg, png, размер слайдов 1920x650px");
	}
	else
	{
		$(this).siblings(".vsbl").val(fileName);
	}
});
/*__________ Shop Seller Slide Input --- __________*/



/*__________ Shop Seller Slide Add +++ __________*/
$(document).on('click', '.slr_shp_stg_sld_add > a', function (e) {
	e.preventDefault();

	$('.slr_shp_stg_sld_bd').append($('.slr_shp_stg_sld_inp_bx')[0].outerHTML);
});
/*__________ Shop Seller Slide Add --- __________*/










/*__________ Mobile Swipe +++ __________*/
$(document).ready(function () {

	var startX, startY;
	$(document).on('touchstart', function (e){
		startX = e.touches[0].clientX;
		startY = e.touches[0].clientY;
	});

	$(document).on('touchmove', function (e){
		var moveX = e.touches[0].clientX;
		var moveY = e.touches[0].clientY;

		var difX = Math.abs(startX - moveX);
		var difY = Math.abs(startY - moveY);




		// Check for Swipe over Slider
		if (!$(e.target).closest('.owl-carousel').hasClass('owl-carousel'))
		{
			if (difX > 20 && difY < 20) // Check for Swipe direction in given area (square)
			{
				if(moveX > startX) // Check for Swipe Right
				{
					$('.wrapper_cnt').addClass('active');
					$('.nav_menu_sec').addClass('active');
					$('.nav_menu_bg').addClass('active');
					$('html, body').addClass('ov-h');
				}
				else if(moveX < startX) // Check for Swipe Left
				{
					$('.wrapper_cnt').removeClass('active');
					$('.nav_menu_sec').removeClass('active');
					$('.nav_menu_bg').removeClass('active');
					$('html, body').removeClass('ov-h');
				}
			}
		}

		startX = moveX; // Increase Swipe speed

	});

});
/*__________ Mobile Swipe --- __________*/




















/*__________ Validation +++ __________*/


